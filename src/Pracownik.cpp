#include "Pracownik.h"

int Pracownik::m_nextID = 0;

Pracownik::Pracownik(const char* im, const char * naz, int dzien, int miesiac, int rok) : m_nIDZatrudnienia(m_nextID++), m_Imie(im), m_Nazwisko(naz), m_DataUrodzenia(dzien, miesiac, rok) {} 

Pracownik::Pracownik(const Pracownik & wzor) : m_Imie(wzor.m_Imie), m_Nazwisko(wzor.m_Nazwisko), m_nIDZatrudnienia(wzor.m_nIDZatrudnienia), m_DataUrodzenia(wzor.m_DataUrodzenia) {}

Pracownik::~Pracownik() {}


Pracownik & Pracownik::operator=(const Pracownik & wzor)
{
    m_Imie = wzor.m_Imie;
    m_Nazwisko = wzor.m_Nazwisko;
    m_DataUrodzenia = wzor.m_DataUrodzenia;

    return *this;
}

int Pracownik::Porownaj(const Pracownik& wzorzec) const
{
    int spr_imie = SprawdzImie(wzorzec.Imie());
    int spr_nazwisko = SprawdzNazwisko(wzorzec.Nazwisko());
    int spr_data = m_DataUrodzenia.Porownaj(wzorzec.m_DataUrodzenia);
    if(!spr_nazwisko)
    {
        if(!spr_imie)
        {
            if(!spr_data)
            {
                return m_nIDZatrudnienia - wzorzec.m_nIDZatrudnienia;
            }
            else return -spr_data;
        }
        else return spr_imie;
    }
    else return spr_nazwisko;
}

const char* Pracownik::Imie() const
{
    return m_Imie.Zwroc();
}

const char* Pracownik::Nazwisko() const
{
    return m_Nazwisko.Zwroc();
}

void Pracownik::Imie(const char* nowe_imie)
{
    m_Imie.Ustaw(nowe_imie);
}

void Pracownik::Nazwisko(const char* nowe_nazwisko)
{
    m_Nazwisko.Ustaw(nowe_nazwisko);
}

void Pracownik::DataUrodzenia(int nowy_dzien, int nowy_miesiac, int nowy_rok)
{
    m_DataUrodzenia.Ustaw(nowy_dzien, nowy_miesiac, nowy_rok);
}

void Pracownik::Wypisz() const
{
    cout << m_Imie << '\t' << m_Nazwisko << '\t' << m_DataUrodzenia;
    cout << std::endl;
}

void Pracownik::Wpisz()
{
    cout << "Podaj imie: " << std::endl;
    cin >> m_Imie;
    cout << "Podaj nazwisko: " << std::endl;
    cin >> m_Nazwisko;
    cout << "Podaj date urodzenia: " << std::endl;
    cin >> m_DataUrodzenia;
}

int Pracownik::SprawdzImie(const char* por_Imie) const
{
    return m_Imie.SprawdzNapis(por_Imie);
}

int Pracownik::SprawdzNazwisko(const char* por_Nazwisko) const
{
    return m_Nazwisko.SprawdzNapis(por_Nazwisko);
}
    
bool Pracownik::operator==(const Pracownik & wzor) const
{
    return !Porownaj(wzor) ? true : false;
}

ostream & operator<<(ostream & wy, const Pracownik & p)
{
    return wy << p.m_Imie << '\t' << p.m_Nazwisko << '\t' << p.m_DataUrodzenia << '\t' << "ID: " << p.m_nIDZatrudnienia;
}

istream & operator>>(istream & we, Pracownik & p)
{
    cout << "Podaj imie: " << std::endl;
    we >> p.m_Imie;
    cout << "Podaj nazwisko: " << std::endl;
    we >> p.m_Nazwisko;
    cout << "Podaj date urodzenia: " << std::endl;
    we >> p.m_DataUrodzenia;

    return we;
}

void Pracownik::WypiszDane()
{
    cout << m_Imie << '\t' << m_Nazwisko << '\t' << m_DataUrodzenia << '\t'<< "ID: " << m_nIDZatrudnienia << endl;
}

Pracownik * Pracownik::KopiaObiektu() const
{
    return new Pracownik( *this );
}


