#include "Pracownik.h"

class Kierownik : public Pracownik 
{
    Napis m_NazwaDzialu;
    int m_nliczbaPracownikow;

    public:
        Kierownik();
        Kierownik(const Kierownik & wzor);

        Kierownik & operator=(const Kierownik & wzor);
        bool operator==(const Kierownik & wzor) const;
        void WypiszDane();
        
        Pracownik * KopiaObiektu() const;
        friend ostream & operator<<(ostream & wy, Kierownik & k);
        friend istream & operator>>(istream & we, Kierownik & k);
};


