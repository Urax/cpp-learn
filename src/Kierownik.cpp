#include "Kierownik.h"

Kierownik::Kierownik() : 
    Pracownik("", "", 1, 1, 1), 
    m_NazwaDzialu(""), 
    m_nliczbaPracownikow(0) 
{}

Kierownik::Kierownik(const Kierownik & wzor) :
    Pracownik(wzor),
    m_NazwaDzialu(wzor.m_NazwaDzialu),
    m_nliczbaPracownikow(wzor.m_nliczbaPracownikow)
{}
    
Kierownik & Kierownik::operator=(const Kierownik & wzor)
{
    if(this != &wzor)
    {
        // new(this)Kierownik(wzor) - kopiuje 1 w 1 obiekt (co za tym idzie tez ID)
        (Pracownik&) *this = wzor;
        m_NazwaDzialu = wzor.m_NazwaDzialu;
        m_nliczbaPracownikow = wzor.m_nliczbaPracownikow;
    }
    return *this;
}

bool Kierownik::operator==(const Kierownik & wzor) const
{
    return 
    (
        !Porownaj(wzor) && 
        (wzor.m_NazwaDzialu == m_NazwaDzialu) &&
        (wzor.m_nliczbaPracownikow == wzor.m_nliczbaPracownikow)
    ) ? true : false;
}

void Kierownik::WypiszDane()
{
    Pracownik& p_tmp = *this;
    cout << p_tmp << '\t'
         << "Kierownik dzialu: " << m_NazwaDzialu << '\t' 
         << "Liczba pracownikow: " << m_nliczbaPracownikow << endl;
}

Pracownik * Kierownik::KopiaObiektu() const
{
    return new Kierownik( *this );
}

istream & operator>>(istream & we, Kierownik & k)
{
    Pracownik& p_tmp = k;
    we >> p_tmp;
    cout << "Podaj nazwe dzialu:" << endl;
    we >> k.m_NazwaDzialu;
    cout << "Podaj liczbe pracownikow:" << endl;
    we >> k.m_nliczbaPracownikow;
    we.ignore(INTMAX_MAX, '\n');
    return we;
}

ostream & operator<<(ostream & wy, Kierownik & k)
{
    Pracownik& p_tmp = k;
    wy << p_tmp << '\t'
       << "Kierownik dzialu: " << k.m_NazwaDzialu << '\t' 
       << "Liczba pracownikow: " << k.m_nliczbaPracownikow << endl;
    return wy;
}

