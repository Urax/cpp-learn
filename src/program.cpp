#include "ListaPracownikow.h"

//#include "Napis.h"

//#include "Pracownik.h"

#include "Kierownik.h"


int main()
{

    /*
     * zad 5:
    */
    /*
    Data d1(1, 2, 3);
    cin >> d1;
    cout << d1 << endl;

    Napis n1, n2;

    cout << "Podaj pierwszy napis: ";
    cin >> n1;

    cout << "Podaj drugi napis: ";
    cin >> n2;

    tabs[0]->data = malloc(sizeof(int)*(tabs[0]->maxLength));
    if( n1 == n2)
    {
        cout << "Napisy sa sobie rowne !" << endl;
    }
    else
    {
        cout << "Napisy sa rozne" << endl;
    }

    Pracownik p1;
    cout << "Podaj dane pracownika 1:" << endl;
    cin >> p1;
    Pracownik p2;
    cout << "Podaj dane pracownika 2:" << endl;
    cin >> p2;

    if( p1 == p2 )
    {
        cout << "Pracownicy sa tacy sami" << endl;
    }
    else
    {
        cout << "Pracownicy sa rozni" << endl;
    }

    Pracownik p3 = p1;
    
    if( p1 == p3 )
    {
        cout << "Tacy sami pracownicy musza miec takie samo ID zatrudnienia !!" << endl;
    }
    */

    Kierownik k;
    Pracownik p1;

    ListaPracownikow lp;
    
    cout << "Podaj dane pracownika:" << endl;
    cin >> p1;
    lp.Dodaj(p1);
    lp.Dodaj(p1);
    // specjalnie wywolano dwa razy funkcje na tym obiekcie

    Pracownik p2 = p1;
    lp.Dodaj(p2);
    Pracownik p3;
    p3 = p1;
    lp.Dodaj(p3);

    cout << "Podaj dane kierownika:" << endl;
    cin >> k;
    lp.Dodaj(k);
    lp.Dodaj(k);

    Kierownik malpa;
    malpa = k;
    lp.Dodaj(malpa);

    lp.WypiszDanePracownikow();

    return 0;
}
