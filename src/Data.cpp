#include "Data.h"


Data::Data(int d, int m, int r)
{
   m_nDzien = d;
   m_nMiesiac = m;
   m_nRok = r;
}


void Data::Ustaw(int d, int m, int r)
{
    m_nDzien = d;
    m_nMiesiac = m;
    m_nRok = r;
    Koryguj();
}

int Data::Dzien() const
{
    return m_nDzien;
}

int Data::Miesiac() const
{
    return m_nMiesiac;
}

int Data::Rok() const
{
    return m_nRok;
}

void Data::Wypisz() const
{
    cout << m_nDzien << '-' << m_nMiesiac << '-' << m_nRok;
}

void Data::Wpisz()
{
    cout << "Podaj dzien: " << endl;
    cin >> m_nDzien;
    cout << "Podaj miesiac: " << endl;
    cin >> m_nMiesiac;
    cout << "Podaj rok: " << endl;
    cin >> m_nRok;
    Koryguj();
}

void Data::Koryguj()
{
    // poprawa miesiaca
    
    if(m_nMiesiac < 1)
        m_nMiesiac = 1;
    else if(m_nMiesiac > 12)
        m_nMiesiac = 12;

    int max_dzien = 31;

    // ustalenie maksymalnego dnia dla danego roku i miesiaca

    if(m_nMiesiac == 2)
        max_dzien = (m_nRok % 4) ? 28 : 29;   
    else if((m_nMiesiac < 8) && (!(m_nMiesiac % 2)))
        max_dzien = 30;
    else if((m_nMiesiac > 8) && (m_nMiesiac % 2))
        max_dzien = 30;
    
    // poprawa dnia

    if(m_nDzien < 1)
        m_nDzien = 1;
    else if(m_nDzien > max_dzien)
        m_nDzien = max_dzien;
}

int Data::Porownaj(const Data & wzor) const
{
    if(wzor.m_nRok == m_nRok)
    {
        if(wzor.m_nMiesiac == m_nMiesiac)
        {
            if(wzor.m_nDzien == m_nDzien)
                return 0;

            else if(wzor.m_nDzien > m_nDzien)
                return 1;

            else
                return -1;
        }
        
        else if(wzor.m_nMiesiac > m_nMiesiac)
            return 1;
        
        else
            return -1;
    }

    else if(wzor.m_nRok > m_nRok)
        return 1;

    else
        return -1;
}

ostream & operator<<(ostream & wy, const Data & d)
{
    return wy << d.m_nDzien << '-' << d.m_nMiesiac << '-' << d.m_nRok;
}

istream & operator>>(istream & we, Data & d)
{
    cout << "Podaj dzien: " << endl;
    we >> d.m_nDzien;
    cout << "Podaj miesiac: " << endl;
    we >> d.m_nMiesiac;
    cout << "Podaj rok: " << endl;
    we >> d.m_nRok;
    d.Koryguj();
    // flushowanie buffora:
    we.ignore(INTMAX_MAX, '\n');
    return we;
}

