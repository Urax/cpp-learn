#pragma once

#include <iostream>
#include <fstream>

using std::cout; using std::endl;
using std::cin;
using std::ostream; using std::istream;

class Data
{
    int m_nDzien;
    int m_nMiesiac;
    int m_nRok;
    void Koryguj();
    public:
        Data(int d, int m, int r);
        void Ustaw(int d, int m, int r);
        int Dzien() const;
        int Miesiac() const;
        int Rok() const;
        void Wypisz() const;
        void Wpisz();
        int Porownaj(const Data & wzor) const;

        friend ostream & operator<<(ostream & wy, const Data & d);
        friend istream & operator>>(istream & we, Data & d);
};
