#include "Napis.h"

Napis::Napis(const char* nap)
{
    this->~Napis();
    while(*(nap+m_nDl) != '\0')
    {
        m_nDl++;
    }
    m_nDl++;

    m_pszNapis = new char[m_nDl];

    int i = 0;
    while( i != m_nDl )
    {
        m_pszNapis[i] = nap[i];
        ++i;
    }
}

Napis::~Napis()
{
    if(this->m_pszNapis != NULL)
    {
        m_nDl = 0;
        delete m_pszNapis;
    }
}

Napis::Napis(const Napis & wzor)
{
    this->~Napis();
    while(*(wzor.m_pszNapis + m_nDl) != '\0')
    {
        m_nDl++;
    }
    m_nDl++;

    this->m_pszNapis = new char[m_nDl];

    int i = 0;
    while( i != m_nDl )
    {
        m_pszNapis[i] = wzor.m_pszNapis[i];
        ++i;
    }
}

Napis & Napis::operator=(const Napis & wzor)
{
    this->~Napis();
    m_nDl = 0;
    while(*(wzor.m_pszNapis + m_nDl) != '\0')
    {
        m_nDl++;
    }
    m_nDl++;

    m_pszNapis = new char[m_nDl];

    int i = 0;
    while( i != m_nDl )
    {
        m_pszNapis[i] = wzor.m_pszNapis[i];
        ++i;
    }
    return *this;
}

const char* Napis::Zwroc() const
{
    return this->m_pszNapis;
}

void Napis::Ustaw(const char* nowy_napis)
{
    this->~Napis();

    while(*(nowy_napis + m_nDl) != '\0')
    {
        m_nDl++;
    }
    m_nDl++;

    this->m_pszNapis = new char[m_nDl];

    int i = 0;
    while( i != m_nDl )
    {
        m_pszNapis[i] = nowy_napis[i];
        ++i;
    }
}

void Napis::Wypisz() const
{
    
    cout << this->m_pszNapis << endl;
}

void Napis::Wpisz()
{
    this->~Napis();
    int m_nDl_max = 2;
    m_pszNapis = new char[m_nDl_max];
    char c;
    char* tmp;
    cin.get(c);
    while(c != '\n')
    {
        m_pszNapis[m_nDl] = c;
        m_nDl++;
        if(m_nDl == m_nDl_max)
        {
            m_nDl_max *= 2;
            tmp = (char *) realloc(m_pszNapis, m_nDl_max*sizeof(char));
            if(tmp)
            {
                m_pszNapis = tmp;
            }
            else
            {
                cout << "Problem z allokacja pamieci" << endl;
            }
        }
        cin.get(c);

    }
    m_pszNapis[m_nDl] = '\0';
}

int Napis::SprawdzNapis(const char* por_napis) const
{
    int diff = 0;

    // dodatnie wartosci gdy napis pozniej alfabetycznie niz w parametrze 
    // ujemne wartosci gdy napis wczesniej alfabetycznie niz w parametrze
    for(int i = -1;(i+1==0) || ((diff==0) && (por_napis[i] != '\0') && (this->m_pszNapis[i] != '\0')) ; ++i, diff = this->m_pszNapis[i] - por_napis[i]);
    return diff;
}



bool Napis::operator==(const Napis & wzor) const
{
    return !SprawdzNapis(wzor.m_pszNapis) ? true : false; 
}

ostream & operator<<(ostream & wy, const Napis & p)
{
    return wy << p.m_pszNapis;
}

istream & operator>>(istream & we, Napis & p)
{
    p.~Napis();
    int m_nDl_max = 2;
    p.m_pszNapis = new char[m_nDl_max];
    char c;
    char* tmp;
    we.get(c);
    while(c != '\n')
    {
        p.m_pszNapis[p.m_nDl] = c;
        p.m_nDl++;
        if(p.m_nDl == m_nDl_max)
        {
            m_nDl_max *= 2;
            tmp = (char *) realloc(p.m_pszNapis, m_nDl_max*sizeof(char));
            if(tmp)
            {
                p.m_pszNapis = tmp;
            }
            else
            {
                cout << "Problem z allokacja pamieci" << endl;
            }
        }
        we.get(c);

    }
    p.m_pszNapis[p.m_nDl] = '\0';
    return we;
}

