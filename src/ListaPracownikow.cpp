
#include "ListaPracownikow.h"

ListaPracownikow::ListaPracownikow()
{
    m_pPoczatek = NULL;
    m_nLiczbaPracownikow = 0;
}

void ListaPracownikow::Dodaj(const Pracownik& p)
{
    Pracownik* nowy_pracownik = p.KopiaObiektu();
    nowy_pracownik->m_pNastepny = NULL;
    Pracownik* szuk_pracownik = m_pPoczatek;


    // unikniecie dodania tego samego obiektu wielokrotnie
    while(szuk_pracownik != NULL)
    {
        if(*szuk_pracownik == p)
            return;
        szuk_pracownik = szuk_pracownik->m_pNastepny;
    }

    szuk_pracownik = m_pPoczatek;

    if(!m_nLiczbaPracownikow)
    {
        m_pPoczatek = nowy_pracownik;
        m_nLiczbaPracownikow++;
    }
    else if (szuk_pracownik->Porownaj(*nowy_pracownik) > 0)
    {
        Pracownik* tmp;
        tmp = m_pPoczatek;
        m_pPoczatek = nowy_pracownik;
        m_pPoczatek->m_pNastepny = tmp;
        m_nLiczbaPracownikow++;
    }
    else 
    {
        int porownanie;
        while (szuk_pracownik->m_pNastepny != NULL)
        {
            porownanie = szuk_pracownik->m_pNastepny->Porownaj(*nowy_pracownik);

            if (porownanie > 0)
            {
                Pracownik* tmp;
                tmp = szuk_pracownik->m_pNastepny;
                szuk_pracownik->m_pNastepny = nowy_pracownik;
                nowy_pracownik->m_pNastepny = tmp;
                break;
            }
            else if (porownanie == 0)
            {
                break;
            }
            szuk_pracownik = szuk_pracownik->m_pNastepny;
        }
        if (szuk_pracownik->m_pNastepny == NULL)
        {
            szuk_pracownik->m_pNastepny = nowy_pracownik;
        }
    }
}

void ListaPracownikow::WypiszPracownikow() const
{
    Pracownik* pom_pomocnik = m_pPoczatek;
    while (pom_pomocnik != NULL)
    {
        pom_pomocnik->Wypisz();
        pom_pomocnik = pom_pomocnik->m_pNastepny;
    }
}
                ;
void ListaPracownikow::Usun(const Pracownik& wzorzec)
{
    Pracownik* przedusuniety = m_pPoczatek;
    if(m_pPoczatek->Porownaj(wzorzec))
    {
        m_pPoczatek = m_pPoczatek->m_pNastepny;
        delete przedusuniety; // usuwany poczatek listy
    }
    else
    {
        while (przedusuniety->m_pNastepny != NULL)
        {
            if (!przedusuniety->m_pNastepny->Porownaj(wzorzec))
            {
                Pracownik* tmp = przedusuniety->m_pNastepny->m_pNastepny;
                delete przedusuniety->m_pNastepny;
                przedusuniety->m_pNastepny = tmp;
                break;
            }
            przedusuniety = przedusuniety->m_pNastepny;
        }
    }
}

const Pracownik* ListaPracownikow::Szukaj(const char* nazwisko, const char* imie) const
{
    Pracownik* szuk_pracownik = m_pPoczatek;
    while (szuk_pracownik != NULL)
    {
        if (!szuk_pracownik->SprawdzImie(imie))
        {
            if (!szuk_pracownik->SprawdzNazwisko(nazwisko))
            {
                return szuk_pracownik;
            }
        }
        
        szuk_pracownik = szuk_pracownik->m_pNastepny;
    }

    return NULL;
}

void ListaPracownikow::WypiszDanePracownikow() const
{
    Pracownik* pom_pracownik = m_pPoczatek;
    while (pom_pracownik != NULL)
    {
        pom_pracownik->WypiszDane();
        pom_pracownik = pom_pracownik->m_pNastepny;
    }

}
