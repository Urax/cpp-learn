#pragma once

#include <iostream>
#include <fstream>

using std::cout; using std::cin; using std::endl;
using std::istream; using std::ostream;

class Napis
{
    char * m_pszNapis = NULL;
    int m_nDl = 0;

    public:
        Napis(const char* nap = "");
        ~Napis();
        Napis(const Napis & wzor);
        Napis & operator=(const Napis & wzor);
        const char* Zwroc() const;
        void Ustaw(const char* nowy_napis);
        void Wypisz() const;
        void Wpisz();
        int SprawdzNapis(const char* por_napis) const;

        bool operator==(const Napis & wzor) const;
        friend ostream & operator<<(ostream & wy, const Napis & p);
        friend istream & operator>>(istream & we, Napis & p);
};
    
