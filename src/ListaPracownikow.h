
#include "Pracownik.h"

class ListaPracownikow
{
    Pracownik* m_pPoczatek;
    int m_nLiczbaPracownikow;

public:
    ListaPracownikow();
    void Dodaj(const Pracownik& p);
    void Usun(const Pracownik& wzorzec);
    void WypiszPracownikow() const;
    void WypiszDanePracownikow() const;
    const Pracownik* Szukaj(const char* nazwisko, const char* imie) const;
};
